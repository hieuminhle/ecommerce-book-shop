import { Injectable } from '@angular/core';
import { CartItem } from '../common/cart-item';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  cartItems: CartItem[] = [];
  totalPrice: Subject<number> = new Subject<number>();
  totalQuantity: Subject<number> = new Subject<number>();

  constructor() { }

  addToCart(theCartItem: CartItem) {
    // Check if we already had the item in our cart:
    let alreadyExistsInCart = false;
    let existingCartItem: CartItem = undefined;

    if (this.cartItems.length > 0) {
      // Find the item in the cart base on the item ID:

      // // Using for loop:
      // for (let cartItem of this.cartItems) {
      //   if (cartItem.id === theCartItem.id) {
      //     existingCartItem = cartItem;
      //     break;
      //   }
      // }

      // Using find() method:
      existingCartItem = this.cartItems.find(cartItem => cartItem.id === theCartItem.id);

      // Check if we found it:
      alreadyExistsInCart = (existingCartItem != undefined);
    }

    if (alreadyExistsInCart) {
      // If item already exists in cart, increment the quantity:
      existingCartItem.quantity++;
    } else {
      // Else, add the item to cart:
      this.cartItems.push(theCartItem);
    }

    // Compute cart total price and total quantity:
    this.computeCartTotals();
  }

  computeCartTotals() {
    let totalPriceValue: number = 0;
    let totalQuantityValue: number = 0;

    for (let currentCartItem of this.cartItems) {
      totalPriceValue += (currentCartItem.quantity * currentCartItem.unitPrice);
      totalQuantityValue += currentCartItem.quantity;
    }

    // Publish the new values... all subscribers will receive the new data:
    this.totalPrice.next(totalPriceValue);
    this.totalQuantity.next(totalQuantityValue);

    // Log cart data for debugging purposes:
    this.logCartData(totalPriceValue, totalQuantityValue);
  }

  logCartData(totalPriceValue: number, totalQuantityValue: number) {
    console.log('Cart content: ');
    for (let cartItem of this.cartItems) {
      const subTotalPrice = cartItem.quantity * cartItem.unitPrice;
      console.log(`name=${cartItem.name}, quantity=${cartItem.quantity}, price=$${cartItem.unitPrice}, subTotalPrice=$${subTotalPrice}`);
    }

    console.log(`totalPrice: $${totalPriceValue.toFixed(2)}, totalQuantity: ${totalQuantityValue}`);
    console.log('--------------------------------------------------');
  }

  decrementQuantity(theCartItem: CartItem) {
    theCartItem.quantity--;
    if (theCartItem.quantity === 0) {
      this.remove(theCartItem);
    } else {
      this.computeCartTotals();
    }
  }

  remove(theCartItem: CartItem) {
    // Get index of item in the array:
    const itemIndex = this.cartItems.findIndex(cartItem => cartItem.id === theCartItem.id);
    // If found, remove the item from the array at index:
    if (itemIndex > -1) {
      this.cartItems.splice(itemIndex, 1); // splice() in this case means remove 1 item at index itemIndex from the array.
      this.computeCartTotals();
    }
  }
}
