package io.hieu.ecommerce.config;

import io.hieu.ecommerce.entities.Product;
import io.hieu.ecommerce.entities.ProductCategory;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.http.HttpMethod;

import javax.persistence.EntityManager;
import javax.persistence.metamodel.EntityType;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Configuration
public class MyDataRestConfig implements RepositoryRestConfigurer {
    private EntityManager entityManager;

    public MyDataRestConfig(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration configuration) {
        HttpMethod[] theUnsupportedActions = {HttpMethod.POST, HttpMethod.PUT, HttpMethod.DELETE};

        // Disabling HTTP methods for Product: POST, PUT, DELETE
        configuration.getExposureConfiguration()
                .forDomainType(Product.class)
                .withItemExposure(((metdata, httpMethods) -> httpMethods.disable(theUnsupportedActions)))
                .withCollectionExposure(((metdata, httpMethods) -> httpMethods.disable(theUnsupportedActions)));

        // Disabling HTTP methods for ProductCategory: POST, PUT, DELETE
        configuration.getExposureConfiguration()
                .forDomainType(ProductCategory.class)
                .withItemExposure(((metdata, httpMethods) -> httpMethods.disable(theUnsupportedActions)))
                .withCollectionExposure(((metdata, httpMethods) -> httpMethods.disable(theUnsupportedActions)));

        // Call an internal helper method to expose entities' Ids on the REST endpoints:
        exposeIds(configuration);
    }

    // exposeIds() method Expose entity Ids:
    private void exposeIds(RepositoryRestConfiguration configuration) {
        // Expose all entity classes from the entity manager:
        Set<EntityType<?>> entities = entityManager.getMetamodel().getEntities();

        // Create an ArrayList of those entity types:
        List<Class> entityClasses = new ArrayList<Class>();

        // Get the entity types for the entities:
        for (EntityType entityType : entities) {
            entityClasses.add(entityType.getJavaType());
        }

        // Expose the entity Ids for the array of entity/domain types:
        Class[] domainTypes = entityClasses.toArray(new Class[0]);
        configuration.exposeIdsFor(domainTypes);
    }
}