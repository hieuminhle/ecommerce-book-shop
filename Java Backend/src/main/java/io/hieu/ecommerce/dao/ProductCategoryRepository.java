package io.hieu.ecommerce.dao;

import io.hieu.ecommerce.entities.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin("http://127.0.0.1:4200")
// By placing the @RepositoryRestResource annotation on top of this interface provides us with the Rest endpoint.  By default, Spring will use the pluralized entity name for the endpoint.
@RepositoryRestResource(collectionResourceRel = "productCategory", path = "product-category")
public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Long> {
}